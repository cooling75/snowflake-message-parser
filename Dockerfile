FROM telegraf:1.24

COPY parser /etc/telegraf/parser

ENTRYPOINT ["/entrypoint.sh"]
CMD ["telegraf"]