package main

import (
	"testing"
)

func TestGetkb_KB(t *testing.T) {
	want := 999
	res := Getkb([]string{"", "KB"}, 999)

	if want != res {
		t.Fatalf(`Getkb("999 KB") = %d, want match for %d `, res, want)
	}
}

func TestGetkb_MB(t *testing.T) {
	want := 99000
	res := Getkb([]string{"", "MB"}, 99)

	if want != res {
		t.Fatalf(`Getkb("999 KB") = %d, want match for %d `, res, want)
	}
}
